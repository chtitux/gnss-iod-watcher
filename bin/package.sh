#!/bin/bash -ex

VERSION=$(date +%Y-%m-%d-%H%M%S)
DEPLOY_DIR="/var/www/gnss-iod-watcher/$VERSION"

# rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

mkdir $DEPLOY_DIR

# Copy git repository to deploy dir
git archive --format=tar --prefix="" HEAD | (cd $DEPLOY_DIR && tar xf -)

cd $DEPLOY_DIR

# Install gems
cp -a /var/www/gnss-iod-watcher/current/vendor/bundle vendor/ || true
bundle config set deployment 'true'
bundle package
bundle install --jobs $(nproc)

# Copy secret
cp -a /var/www/gnss-iod-watcher/master.key $DEPLOY_DIR/config/

# Build assets
make assets

chown -R ruby-prod $DEPLOY_DIR

rm /var/www/gnss-iod-watcher/current
ln -s $DEPLOY_DIR /var/www/gnss-iod-watcher/current
