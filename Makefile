assets:
	RAILS_ENV=production bundle exec rails assets:precompile

run:
	bundle exec rails server --port 3050

deploy:
	sudo -u ruby-prod bin/package.sh
	sudo systemctl restart gnss-iod-watcher-sidekiq.service
	sudo systemctl restart gnss-iod-watcher-puma.service

docker-run:
	docker run -ti -e PORT=3020 -e CDN_ASSETS_HOST="cdn.example.com" -e CDN_ASSETS_PREFIX="assets/" -e GNSS_POSTGRES_PORT=${GNSS_POSTGRES_PORT} -e GNSS_POSTGRES_USER=${GNSS_POSTGRES_USER} -e GNSS_POSTGRES_PASSWORD=${GNSS_POSTGRES_PASSWORD} -e GNSS_POSTGRES_HOST=/var/run/postgresql/ -p 8080:3020 -v /var/run/postgresql/.s.PGSQL.5436:/var/run/postgresql/.s.PGSQL.5436 gnss2:latest

docker-build:
	docker build -t gnss2 .

gcloud-build:
	gcloud builds submit --config cloudbuild.yaml --substitutions=_RAILS_MASTER_KEY="$$(cat config/master.key)"

gcloud-deploy:
	gcloud run deploy --image gcr.io/gnss-iod-watcher/gnss-iod-watcher --platform managed

edit-secrets:
	bundle exec rails credentials:edit
