# Start from scratch
FROM ruby:2.7-slim
RUN apt-get update \
  && apt-get install -y libpq5 libcurl4 ca-certificates make

WORKDIR /app

ENV BUNDLE_PATH=/app/vendor/bundle
ENV RAILS_ENV=production

COPY . ./

COPY ./artifacts/vendor vendor
COPY ./artifacts/public public
COPY ./artifacts/bundle_config /usr/local/bundle/config

RUN ls -al
RUN ls -al vendor/
RUN ls -al public/
RUN find public/

# Run the web service on container startup.
CMD ["bundle", "exec", "puma"]
