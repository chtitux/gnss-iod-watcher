schedule_file = "config/schedule.yml"
Sidekiq.configure_server do |config|
  config.redis = { password: ENV["GNSS_REDIS_PASSWORD"] }
end

Sidekiq.configure_client do |config|
  config.redis = { password: ENV["GNSS_REDIS_PASSWORD"] }
end

if File.exist?(schedule_file) && Sidekiq.server?
  Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file)
end

