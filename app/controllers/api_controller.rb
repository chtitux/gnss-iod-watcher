class ApiController < ApplicationController
  UPLINK_STATIONS = [
    { coord: [-17.5386449, -149.6194316], name: "Papeete" },
    { coord: [  5.1648168,  -52.6596651], name: "Kourou"  },
    { coord: [ 78.2404686,   15.7969334], name: "Svalbard"},
    { coord: [-20.9115274,   55.5131576], name: "Réunion" },
    { coord: [-22.2642742,  166.4098473], name: "Nouméa"  },
  ].freeze

  def uplink_stations
    render json: UPLINK_STATIONS
  end

  def satellite_vehicles
    satellite_vehicles = SatelliteVehicle
      .where(system: params[:system])
      .where.not(iod: nil)
      .where.not(latitude: nil)
      .where.not(longitude: nil)
      .order(name: :asc)
      .includes(:update_events)

    satellite_vehicles_json = satellite_vehicles.map do |satellite_vehicle|
      {
        name:       satellite_vehicle.name,
        coord:      [satellite_vehicle.latitude, satellite_vehicle.longitude],
        iod:        satellite_vehicle.iod,
        iod_age:    satellite_vehicle.iod_age.to_i,
        updated_at: satellite_vehicle.updated_at,
      }
    end

    render json: satellite_vehicles_json
  end

  def update_events
    last_hours = params[:last_hours].try(:to_i) || 2

    update_events = UpdateEvent
      .joins(:satellite_vehicle)
      .includes(:satellite_vehicle)
      .where(satellite_vehicles: { system: params[:system] })
      .where(created_at: last_hours.hours.ago..)

    events_json = update_events.map do |event|
      {
        c: [ event.latitude, event.longitude ],
        sv: event.satellite_vehicle.name,
        iod: event.iod,
        at: event.created_at,
      }
    end

    render json: events_json
  end
end
