class HomeController < ApplicationController
  DEFAULT_SYSTEM = :galileo
  SYSTEMS = [
    :galileo,
    :beidou,
    :glonass,
    :gps,
  ]

  def index
    @systems = SYSTEMS
    @system = params[:system]
    @last_hours = params[:last_hours]

    if !@system.present?
      redirect_to home_index_path(system: DEFAULT_SYSTEM)
    end

    @svs = SatelliteVehicle
      .where(system: @system)
      .where.not(iod: nil)
      .order(name: :asc)
      .includes(:update_events)
  end
end
