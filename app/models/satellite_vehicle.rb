class SatelliteVehicle < ApplicationRecord
  has_many :update_events, -> { order(:created_at) }

  def iod_age
    Time.current - last_iod_update
  end

  def last_iod_update
    update_events.last.created_at
  end

  def css_class
    if iod_age > 3600
      'stale-3600'
    elsif iod_age > 1800
      'stale-1800'
    elsif iod_age > 700
      'stale-700'
    end
  end
end
