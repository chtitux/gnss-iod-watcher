class FetchIodWorker
  include Sidekiq::Worker

  SVS_URL = "https://galmon.eu/svs.json"
  ALMANAC_URL = "https://galmon.eu/almanac.json"

  def perform
    svs_json_data = JSON.parse(Typhoeus.get(SVS_URL).body)
    almanac_json_data = JSON.parse(Typhoeus.get(ALMANAC_URL).body)

    svs_json_data.each do |sv_full_name, sv_json|
      sv_name = sv_json['name']
      sv = SatelliteVehicle.find_or_create_by(name: sv_name)

      sv_almanac = almanac_json_data[sv_name]
      # Skip if we don't have the position
      if sv_almanac.nil?
        next
      end

      # Skip if observation is outdated
      if sv_json["last-seen-s"] >= 600
        next
      end

      # Skip Galileo E5a/E5b signals as we don't receive them properly
      if sv_json['fullName'].match?(/@(5|6)$/)
        next
      end
    
      sv_latitude  = sv_almanac['eph-latitude']
      sv_longitude = sv_almanac['eph-longitude']

      current_iod = sv_json['iod'].to_s
      if sv.iod != current_iod
        last_update_event = sv.update_events.order(:id).last
    
        if last_update_event.present?
          previous_iod_age = Time.current - last_update_event.created_at
        end
    
        sv.update_events.create(
          iod: current_iod,
          previous_iod_age: previous_iod_age,
          latitude: sv_latitude,
          longitude: sv_longitude,
        )
      end

      sv.update(
        iod: current_iod,
        latitude: sv_latitude,
        longitude: sv_longitude,
      )

    end
  end
end
