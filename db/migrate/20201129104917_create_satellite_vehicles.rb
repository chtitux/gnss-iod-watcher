class CreateSatelliteVehicles < ActiveRecord::Migration[6.0]
  def change
    create_table :satellite_vehicles do |t|
      t.string :name
      t.string :system
      t.string :iod
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
    add_index :satellite_vehicles, :name, unique: true
  end
end
