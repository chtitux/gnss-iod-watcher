class CreateUpdateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :update_events do |t|
      t.references :satellite_vehicle, null: false, foreign_key: true
      t.string :iod
      t.integer :previous_iod_age
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
