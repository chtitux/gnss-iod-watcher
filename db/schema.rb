# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_29_105815) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "satellite_vehicles", force: :cascade do |t|
    t.string "name"
    t.string "system"
    t.string "iod"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_satellite_vehicles_on_name", unique: true
  end

  create_table "update_events", force: :cascade do |t|
    t.bigint "satellite_vehicle_id", null: false
    t.string "iod"
    t.integer "previous_iod_age"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["satellite_vehicle_id"], name: "index_update_events_on_satellite_vehicle_id"
  end

  add_foreign_key "update_events", "satellite_vehicles"
end
